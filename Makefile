DMRFLAGS        = -ldmr
FLAGS           = -O3 -Wall -I$(C_INLCUDE_PATH) -L$(LD_LIBRARY_PATH)
MPIFLAGS    = -lmpi
MKLFLAGS    = -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm

#all: cg_malleable cg_orig cg_moldable
all: clean cg-malleable

print-%  : ; @echo $* = $($*)

cg-orig: cg-orig.c
	mpic++ $(FLAGS) $(MPIFLAGS) $(MKLFLAGS) -fopenmp cg-orig.c -o cg-orig

cg-malleable: cg-malleable.c
	mpic++ $(FLAGS) $(MPIFLAGS) $(DMRFLAGS) $(MKLFLAGS) -fopenmp cg-malleable.c -o cg-malleable

clean:
	rm -f *.out *.o cg-malleable *.dmr cg-orig

