#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl_blas.h>
#include <mpi.h>
#include "reloj.c"

#define MAXITER 10
//#define SIZE 32768
#define SIZE 256
#define OPENMP 0

//mpirun -n 4 ./cg-malleable

// ================================================================================

void CreateDoubles(double **vdouble, int size) {
    if ((*vdouble = (double *) malloc(sizeof (double)*size)) == NULL) {
        printf("Memory Error (CreateDoubles(%d))\n", size);
        exit(1);
    }
}

void RemoveDoubles(double **vdouble) {
    free(*vdouble);
    *vdouble = NULL;
}

void InitDoubles(double *vdouble, int n, double frst, double incr) {
    int i;
    double *p1 = vdouble, num = frst;

    for (i = 0; i < n; i++) {
        *(p1++) = num;
        num += incr;
    }
}

void GenerateDistributeMatrix(double ***pmat, int sizeR, int sizeC) {
    const int max = 10, off = 0, div = 5000;
    int i;
    double **mat = NULL;

    mat = (double **) malloc(sizeR * sizeof (double *));
    mat[0] = (double *) malloc(sizeR * sizeC * sizeof (double));
    for (i = 1; i < sizeR; i++) mat[i] = mat[i - 1] + sizeC;

    for (i = 0; i < sizeR * sizeC; i++)
        mat[0][i] = ((rand() % max) - off) / (1.0 * div);

    *pmat = mat;
}

void PrintMatrixDoubles(double **pmat, int sizeR, int sizeC) {
    int i, j;

    for (i = 0; i < sizeR; i++) {
        for (j = 0; j < sizeC; j++) {
            printf("%10.5f, ", pmat[i][j]);
        }
        printf("\n");
    }
}

void RemoveMatrixDoubles(double ***mdouble) {
    free((*mdouble)[0]);
    free(*mdouble);
    *mdouble = NULL;
}

void AddMatrixVector(double **mat, int sizeR, int sizeC, double *b) {
    int i, j;
    double sum;

    for (i = 0; i < sizeR; i++) {
        sum = 0.0;
        for (j = 0; j < sizeC; j++) {
            sum += mat[i][j];
        }
        b[i] = sum;
    }

}

// ================================================================================

void ProdMatrixVector_Par(double **mat, int sizeR, int sizeC, double *x, double *b) {
    int i, j;
    double sum;

#pragma omp parallel for private(j, sum)
    for (i = 0; i < sizeR; i++) {
        sum = 0.0;
        for (j = 0; j < sizeC; j++) {
            sum += mat[i][j] * x[j];
        }
        b[i] = sum;
    }
}

// ================================================================================
// ========================= FLAT =================================================
// ================================================================================

void GenerateDistributeMatrixFlat(double **pmat, int sizeR, int sizeC) {
    const int max = 10, off = 0, div = 5000;
    int i;
    double *mat = NULL;

    mat = (double *) malloc(sizeR * sizeC * sizeof (double));

    for (i = 0; i < sizeR * sizeC; i++)
        mat[i] = ((rand() % max) - off) / (1.0 * div);

    *pmat = mat;
}

void ProdMatrixVectorFlat(double *mat, int sizeR, int sizeC, double *x, double *b) {
    int i, j;
    double sum;

    if (OPENMP) {
#pragma omp parallel for private(j, sum)
        for (i = 0; i < sizeR; i++) {
            sum = 0.0;
            for (j = 0; j < sizeC; j++) {
                sum += mat[sizeC * i + j] * x[j];
            }
            b[i] = sum;
        }
    } else {
        for (i = 0; i < sizeR; i++) {
            sum = 0.0;
            for (j = 0; j < sizeC; j++) {
                sum += mat[sizeC * i + j] * x[j];
            }
            b[i] = sum;
        }
    }
}

void AddMatrixVectorFlat(double *mat, int sizeR, int sizeC, double *b) {
    int i, j;
    double sum;

    for (i = 0; i < sizeR; i++) {
        sum = 0.0;
        for (j = 0; j < sizeC; j++) {
            sum += mat[sizeC * i + j];
        }
        b[i] = sum;
    }

}

// ================================================================================
// ================================================================================
// ================================================================================

void ConjugateGradient(double *mat, double *x,
        double* res, double *d, double *z, double *aux, int iter) {
    //sleep(2);
    int i;
    int IZERO = 0, IONE = 1;
    double DONE = 1.0, DMONE = -1.0, DZERO = 0.0;
    int n, n_dist, maxiter = MAXITER;
    double tol, rho, alpha, umbral;
    //double *res = NULL, *z = NULL, *d = NULL, *y = NULL;
    double *y = NULL; //, *aux = NULL;
    double t1, t2, t3, t4;
    double p1, p2, p3, p4, pp1 = 0.0, pp2 = 0.0;

    int myId, numProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);

    int size = SIZE;
    int sizeC = size;
    int sizeR = size / numProcs;

    //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - Size: %d, Size per process: %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, size, sizeR);

    int *sizes = (int *) malloc(numProcs * sizeof (int));
    int *dspls = (int *) malloc(numProcs * sizeof (int));
    MPI_Allgather(&sizeR, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
    dspls[0] = 0;
    for (i = 1; i < numProcs; i++) dspls[i] = dspls[i - 1] + sizes[i - 1];
    //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);

    n = size;
    n_dist = sizeR;
    umbral = 1.0e-8; // maxiter = size;
    // tol = norm (res)
    y = res;
    double beta = ddot(&n_dist, res, &IONE, y, &IONE); // beta = res' * y
    MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
    tol = sqrt(beta);
    if (iter < maxiter) {
        MPI_Allgatherv(d, sizeR, MPI_DOUBLE, aux, sizes, dspls, MPI_DOUBLE, MPI_COMM_WORLD);
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
        ProdMatrixVectorFlat(mat, sizeR, sizeC, aux, z); // z = A * d
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
        if (myId == 0) printf("(%d,%20.10e)\n", iter, tol);
        rho = ddot(&n_dist, d, &IONE, z, &IONE);
        MPI_Allreduce(MPI_IN_PLACE, &rho, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        rho = beta / rho;
        daxpy(&n_dist, &rho, d, &IONE, x, &IONE); // x += rho * d;
        rho = -rho;
        daxpy(&n_dist, &rho, z, &IONE, res, &IONE); // res -= rho * z
        //		dcopy (&n_dist, res, &IONE, y, &IONE);                   				// y = res
        y = res;
        alpha = beta; // alpha = beta
        //		beta = ddot (&n_dist, res, &IONE, y, &IONE);                    // beta = res' * y
        beta = ddot(&n_dist, res, &IONE, y, &IONE);
        MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        alpha = beta / alpha; // alpha = beta / alpha
        dscal(&n_dist, &alpha, d, &IONE); // d = alpha * d
        daxpy(&n_dist, &DONE, y, &IONE, d, &IONE); // d += y
        //		tol = dnrm2 (&n_dist, res, &IONE);                              // tol = norm (res)
        //tol = sqrt(*beta);
        iter++;
    } else {
        return;
    }

    ConjugateGradient(mat, x, res, d, z, aux, iter);

    //	RemoveDoubles (&aux); RemoveDoubles (&y); RemoveDoubles (&res); RemoveDoubles (&z); RemoveDoubles (&d);
    //RemoveDoubles(&aux);
    //RemoveDoubles(&res);
    //RemoveDoubles(&z);
    //RemoveDoubles(&d);
}

int main(int argc, char *argv[]) {
    int i, val, ret = 0;
    //	int *posd = NULL;
    double norm = 0.0;
    //	double *x = NULL, *b1 = NULL, *b2 = NULL, *diags = NULL;
    double *x = NULL, *b = NULL;
    double *mat = NULL;
    //	SparseMatrix mat, sym;

    int myId, numProcs;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    printf("Process %d of %d\n", myId, numProcs);

    srand(myId + 1);

    //if (argc != 2) {
    //	printf ("ERROR: CG_Def size \n"); res = -1;
    //} else {
    int IONE = 1;
    double beta = 0.0;

    int size, sizeR, sizeC;
    int *sizes = NULL, *dspls = NULL;
    size = SIZE;
    //size = atoi (argv[1]);
    //		sizeR = (size + (numProcs - 1)) / numProcs;
    sizeR = size / numProcs;
    if ((size % numProcs) > myId) {
        sizeR++;
    }
    //		printf ("sizeR %d\n", sizeR);
    sizeC = size;
    sizes = (int *) malloc(numProcs * sizeof (int));
    dspls = (int *) malloc(numProcs * sizeof (int));
    MPI_Allgather(&sizeR, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
    dspls[0] = 0;
    for (i = 1; i < numProcs; i++) dspls[i] = dspls[i - 1] + sizes[i - 1];

    //	Begin of CG
    GenerateDistributeMatrixFlat(&mat, sizeR, sizeC);

    double *aux, *res, *z, *d;
    CreateDoubles(&aux, size);
    CreateDoubles(&res, sizeR);
    CreateDoubles(&z, sizeR);
    CreateDoubles(&d, sizeR); // CreateDoubles (&y, n_dist);

    CreateDoubles(&x, sizeR);
    CreateDoubles(&b, sizeR);
    InitDoubles(x, sizeR, 0.0, 0.0);
    InitDoubles(b, sizeR, 0.0, 0.0);

    // A*[1,1, ... 1]'= b
    AddMatrixVectorFlat(mat, sizeR, sizeC, b);
    // Transform A as a dominant diagonal matrix, the less dominant, the less likely to converge, so increase the divider (100)
    for (i = 0; i < sizeR; i++) {
        mat[sizeC * i + i + dspls[myId]] += b[i] / 100;
        b[i] += b[i] / 100;
    }

    double DMONE = -1.0;
    int n_dist = sizeR;
    MPI_Allgatherv(x, sizeR, MPI_DOUBLE, aux, sizes, dspls, MPI_DOUBLE, MPI_COMM_WORLD);
    ProdMatrixVectorFlat(mat, sizeR, sizeC, aux, z);
    dcopy(&n_dist, b, &IONE, res, &IONE); // res = b
    daxpy(&n_dist, &DMONE, z, &IONE, res, &IONE); // res -= z
    dcopy(&n_dist, res, &IONE, d, &IONE); // d = y
    //beta = ddot(&n_dist, res, &IONE, res, &IONE); // beta = res' * y
    //MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    ConjugateGradient(mat, x, res, d, z, aux, 0);
    //int tol = sqrt(beta);
    //if (myId == 0)
    //    printf("Fin(%d) --> (%d,%20.10e)\n", size, MAXITER, tol);


    // Error computation
    for (i = 0; i < sizeR; i++) x[i] -= 1.0;
    beta = ddot(&sizeR, x, &IONE, x, &IONE);
    MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    beta = sqrt(beta);
    if (myId == 0) printf("error = %10.5e\n", beta);

    RemoveDoubles(&aux);
    RemoveDoubles(&res);
    RemoveDoubles(&d);
    RemoveDoubles(&z);

    RemoveDoubles(&b);
    RemoveDoubles(&x);
    RemoveDoubles(&mat);
    //RemoveMatrixDoubles(&mat);
    //	End of CG
    free(sizes);
    sizes = NULL;
    //}

    MPI_Finalize();

    return ret;
}

