#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl_blas.h>
#include <mpi.h>
#include "reloj.c"
#include "omp.h"
#include "dmr.h"

#define ITERATIONS 10
// #define SIZE 32768
#define SIZE 256
#define OPENMP 0

// mpirun -n 4 ./cg-malleable

// ================================================================================

void CreateDoubles(double **vdouble, int size)
{
    if ((*vdouble = (double *)malloc(sizeof(double) * size)) == NULL)
    {
        printf("Memory Error (CreateDoubles(%d))\n", size);
        exit(1);
    }
}

void RemoveDoubles(double **vdouble)
{
    free(*vdouble);
    *vdouble = NULL;
}

void InitDoubles(double *vdouble, int n, double frst, double incr)
{
    int i;
    double *p1 = vdouble, num = frst;

    for (i = 0; i < n; i++)
    {
        *(p1++) = num;
        num += incr;
    }
}

void GenerateDistributeMatrix(double ***pmat, int sizeR, int sizeC)
{
    const int max = 10, off = 0, div = 5000;
    int i;
    double **mat = NULL;

    mat = (double **)malloc(sizeR * sizeof(double *));
    mat[0] = (double *)malloc(sizeR * sizeC * sizeof(double));
    for (i = 1; i < sizeR; i++)
        mat[i] = mat[i - 1] + sizeC;

    for (i = 0; i < sizeR * sizeC; i++)
        mat[0][i] = ((rand() % max) - off) / (1.0 * div);

    *pmat = mat;
}

void PrintMatrixDoubles(double **pmat, int sizeR, int sizeC)
{
    int i, j;

    for (i = 0; i < sizeR; i++)
    {
        for (j = 0; j < sizeC; j++)
        {
            printf("%10.5f, ", pmat[i][j]);
        }
        printf("\n");
    }
}

void RemoveMatrixDoubles(double ***mdouble)
{
    free((*mdouble)[0]);
    free(*mdouble);
    *mdouble = NULL;
}

void AddMatrixVector(double **mat, int sizeR, int sizeC, double *b)
{
    int i, j;
    double sum;

    for (i = 0; i < sizeR; i++)
    {
        sum = 0.0;
        for (j = 0; j < sizeC; j++)
        {
            sum += mat[i][j];
        }
        b[i] = sum;
    }
}

// ================================================================================

void ProdMatrixVector_Par(double **mat, int sizeR, int sizeC, double *x, double *b)
{
    int i, j;
    double sum;

#pragma omp parallel for private(j, sum)
    for (i = 0; i < sizeR; i++)
    {
        sum = 0.0;
        for (j = 0; j < sizeC; j++)
        {
            sum += mat[i][j] * x[j];
        }
        b[i] = sum;
    }
}

// ================================================================================
// ========================= FLAT =================================================
// ================================================================================

void GenerateDistributeMatrixFlat(double **pmat, int sizeR, int sizeC)
{
    const int max = 10, off = 0, div = 5000;
    int i;
    double *mat = NULL;

    mat = (double *)malloc(sizeR * sizeC * sizeof(double));

    for (i = 0; i < sizeR * sizeC; i++)
        mat[i] = ((rand() % max) - off) / (1.0 * div);

    *pmat = mat;
}

void ProdMatrixVectorFlat(double *mat, int sizeR, int sizeC, double *x, double *b)
{
    int i, j;
    double sum;

    if (OPENMP)
    {
#pragma omp parallel for private(j, sum)
        for (i = 0; i < sizeR; i++)
        {
            sum = 0.0;
            for (j = 0; j < sizeC; j++)
            {
                sum += mat[sizeC * i + j] * x[j];
            }
            b[i] = sum;
        }
    }
    else
    {
        for (i = 0; i < sizeR; i++)
        {
            sum = 0.0;
            for (j = 0; j < sizeC; j++)
            {
                sum += mat[sizeC * i + j] * x[j];
            }
            b[i] = sum;
        }
    }
}

void AddMatrixVectorFlat(double *mat, int sizeR, int sizeC, double *b)
{
    int i, j;
    double sum;

    for (i = 0; i < sizeR; i++)
    {
        sum = 0.0;
        for (j = 0; j < sizeC; j++)
        {
            sum += mat[sizeC * i + j];
        }
        b[i] = sum;
    }
}

// ================================================================================
// ================================================================================
// ================================================================================

void ConjugateGradient(double *mat, double *x,
                       double *res, double *d, double *z,
                       int sizeR, int *sizes, int *dspls)
{
    //int test;
    //MPI_Comm_size(DMR_INTERCOMM, &test);  
    //printf("[%d/%d] %d: %s(%s,%d) @@@@@@@@@@@@@@ %d\n",
    //               DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, test);
    
    int IONE = 1;
    double DONE = 1.0;
    double *y;
    double rho, alpha;
    int n_dist;
    n_dist = sizeR;
    y = res;
    double aux2 = ddot(&n_dist, res, &IONE, y, &IONE); // beta = res' * y
    double *aux= (double*)malloc(sizeof(double) * SIZE);
    double *beta = (double *)malloc(sizeof(double));
    //printf("aux=%p - %p\n", aux, DMR_INTERCOMM);
    MPI_Allreduce(&aux2, beta, 1, MPI_DOUBLE, MPI_SUM, DMR_INTERCOMM);
    MPI_Allgatherv(d, sizeR, MPI_DOUBLE, aux, sizes, dspls, MPI_DOUBLE, DMR_INTERCOMM);
    ProdMatrixVectorFlat(mat, sizeR, SIZE, aux, z); // z = A * d
    rho = ddot(&n_dist, d, &IONE, z, &IONE);
    MPI_Allreduce(MPI_IN_PLACE, &rho, 1, MPI_DOUBLE, MPI_SUM, DMR_INTERCOMM);
    rho = *beta / rho;
    daxpy(&n_dist, &rho, d, &IONE, x, &IONE); // x += rho * d;
    rho = -rho;
    daxpy(&n_dist, &rho, z, &IONE, res, &IONE); // res -= rho * z
    y = res;
    alpha = *beta; // alpha = beta
    *beta = ddot(&n_dist, res, &IONE, y, &IONE);
    MPI_Allreduce(MPI_IN_PLACE, beta, 1, MPI_DOUBLE, MPI_SUM, DMR_INTERCOMM);
    alpha = *beta / alpha;                      // alpha = beta / alpha
    dscal(&n_dist, &alpha, d, &IONE);          // d = alpha * d
    daxpy(&n_dist, &DONE, y, &IONE, d, &IONE); // d += y
}

void remove_data(double **mat, double **x, double **res, double **d,
                 double **z, double **b)
{
    RemoveDoubles(res);
    RemoveDoubles(d);
    RemoveDoubles(z);
    RemoveDoubles(b);
    RemoveDoubles(x);
    RemoveDoubles(mat);
}

void initialize_data(double **mat, double **x, double **res, double **d,
                     double **z, double **b)
{
    int IONE = 1;
    double DMONE = -1.0;
    int myId, numProcs;

    MPI_Comm_rank(DMR_INTERCOMM, &myId);
    MPI_Comm_size(DMR_INTERCOMM, &numProcs);
    //printf("Process %d of %d\n", myId, numProcs);
    srand(myId + 1);

    int size, sizeR, sizeC;
    int *sizes = NULL, *dspls = NULL;
    size = SIZE;
    sizeR = size / numProcs;
    if ((size % numProcs) > myId)
    {
        sizeR++;
    }
    sizeC = size;
    sizes = (int *)malloc(numProcs * sizeof(int));
    dspls = (int *)malloc(numProcs * sizeof(int));
    MPI_Allgather(&sizeR, 1, MPI_INT, sizes, 1, MPI_INT, DMR_INTERCOMM);
    dspls[0] = 0;
    for (int i = 1; i < numProcs; i++)
        dspls[i] = dspls[i - 1] + sizes[i - 1];

    GenerateDistributeMatrixFlat(mat, sizeR, sizeC);

    CreateDoubles(res, sizeR);
    CreateDoubles(z, sizeR);
    CreateDoubles(d, sizeR); // CreateDoubles (&y, n_dist);

    CreateDoubles(x, sizeR);
    CreateDoubles(b, sizeR);
    InitDoubles(*x, sizeR, 0.0, 0.0);
    InitDoubles(*b, sizeR, 0.0, 0.0);
    AddMatrixVectorFlat(*mat, sizeR, sizeC, *b);

    for (int i = 0; i < sizeR; i++)
    {
        (*mat)[sizeC * i + i + dspls[myId]] += (*b)[i] / 100;
        (*b)[i] += (*b)[i] / 100;
    }

    int n_dist = sizeR;
    double *aux= (double*)malloc(sizeof(double) * SIZE);
    MPI_Allgatherv((*x), sizeR, MPI_DOUBLE, aux, sizes, dspls, MPI_DOUBLE, DMR_INTERCOMM);
    ProdMatrixVectorFlat((*mat), sizeR, sizeC, aux, (*z));
    dcopy(&n_dist, (*b), &IONE, (*res), &IONE);         // res = b
    daxpy(&n_dist, &DMONE, (*z), &IONE, (*res), &IONE); // res -= z
    dcopy(&n_dist, (*res), &IONE, (*d), &IONE);         // d = y
}

void gather(MPI_Comm comm, double *send_data, double **recv_data)
{
    int send_size, comm_size, rank;
    // printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__);
    MPI_Comm_size(comm, &comm_size);
    MPI_Comm_rank(comm, &rank);
    send_size = SIZE / comm_size;
    if (rank == 0)
    {
        (*recv_data) = (double *)malloc(SIZE * sizeof(double));
    }
    MPI_Gather(send_data, send_size, MPI_DOUBLE, (*recv_data), send_size, MPI_DOUBLE, 0, comm);
    MPI_Barrier(comm);
}

void scatter(MPI_Comm comm, double **send_data, double **recv_data)
{
    int recv_size, comm_size, rank;
    // printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__);
    MPI_Comm_size(comm, &comm_size);
    MPI_Comm_rank(comm, &rank);
    recv_size = SIZE / comm_size;
    (*recv_data) = (double *)malloc(recv_size * sizeof(double));
    MPI_Scatter((*send_data), recv_size, MPI_DOUBLE, (*recv_data), recv_size, MPI_DOUBLE, 0, comm);
    if (DMR_comm_rank == 0)
    {
        free(*send_data);
    }
    // printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__);
    // MPI_Barrier(DMR_INTERCOMM);
}

/* Executed only by existent ranks */
void send_expand(double **vector)
{
    double *gathered_data = NULL;
    /* Step 1: Gather on old_comm (to rank 0) */
    gather(DMR_COMM_OLD, *vector, &gathered_data);

    /* Step 2: Scatter on new comm (from rank 0) */
    scatter(DMR_COMM_NEW, &gathered_data, vector);
}

/* Executed only by newly added ranks */
void recv_expand(double **vector)
{
    double *dummy_ptr = NULL;
    /* Step 1: Scatter on new comm (from rank 0) */
    scatter(DMR_COMM_NEW, &dummy_ptr, vector);
}

/* Executed only by leaving processes */
void send_shrink(double **vector)
{
    double *dummy_ptr = NULL;
    /* Step 1: Gather on old_comm (to rank 0) */
    gather(DMR_COMM_OLD, *vector, &dummy_ptr);
}

/* Executed only by staying processes */
void recv_shrink(double **vector)
{

    double *gathered_data = NULL;
    /* Step 1: Gather on old_comm (to rank 0) */
    gather(DMR_COMM_OLD, *vector, &gathered_data);

    /* Step 2: Scatter on new comm (from rank 0) */
    scatter(DMR_COMM_NEW, &gathered_data, vector);
}

void init_matrix(MPI_Comm comm, double ** mat){
    int comm_size, myId;
    MPI_Comm_size(comm, &comm_size);
    MPI_Comm_rank(comm, &myId);
    int sizeC = SIZE;
    int sizeR = SIZE / comm_size;
    if ((SIZE % comm_size) > myId)
    {
        sizeR++;
    }

    
    GenerateDistributeMatrixFlat(mat, sizeR, sizeC);
}

/* Executed only by existent ranks */
void send_expand_all(double **mat, double **x, double **res, double **d, double **z, double **b)
{
    free(*mat);
    init_matrix(DMR_COMM_NEW, mat);
    send_expand(x);
    send_expand(res);
    send_expand(d);
    send_expand(z);
    send_expand(b);
}

/* Executed only by newly added ranks */
void recv_expand_all(double **mat, double **x, double **res, double **d, double **z, double **b)
{
    init_matrix(DMR_COMM_NEW, mat);
    recv_expand(x);
    recv_expand(res);
    recv_expand(d);
    recv_expand(z);
    recv_expand(b);
}

/* Executed only by leaving processes */
void send_shrink_all(double **mat, double **x, double **res, double **d, double **z, double **b)
{ 
    send_shrink(x);
    send_shrink(res);
    send_shrink(d);
    send_shrink(z);
    send_shrink(b);
}

/* Executed only by staying processes */
void recv_shrink_all(double **mat, double **x, double **res, double **d, double **z, double **b)
{
    free(*mat);
    init_matrix(DMR_COMM_NEW, mat);
    recv_shrink(x);
    recv_shrink(res);
    recv_shrink(d);
    recv_shrink(z);
    recv_shrink(b);
}

int main(int argc, char *argv[])
{
    double *x = NULL, *b = NULL;
    double *mat = NULL, *z = NULL, *d = NULL, *res = NULL;

    DMR_INIT(ITERATIONS, initialize_data(&mat, &x, &res, &d, &z, &b), recv_expand_all(&mat, &x, &res, &d, &z, &b));
    DMR_Set_parameters(8, 32, 16);

    for (; DMR_it < ITERATIONS + 1; DMR_it++)
    {
        int sizeR = SIZE / DMR_comm_size;
        int *sizes = (int *)malloc(DMR_comm_size * sizeof(int));
        int *dspls = (int *)malloc(DMR_comm_size * sizeof(int));
        MPI_Allgather(&sizeR, 1, MPI_INT, sizes, 1, MPI_INT, DMR_INTERCOMM);
        dspls[0] = 0;
        for (int j = 1; j < DMR_comm_size; j++)
            dspls[j] = dspls[j - 1] + sizes[j - 1];

        if (DMR_comm_rank == 0)
            printf("[%d/%d] %d: %s(%s,%d) PROCESSING STEP %d\n",
                   DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, DMR_it);
        //MPI_Barrier(DMR_INTERCOMM);
        // if (DMR_comm_rank == 0)
        //    printf("[%d/%d] %d: %s(%s,%d) size %d\n",
        //           DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, sizeR);

        ConjugateGradient(mat, x, res, d, z, sizeR, sizes, dspls);
        DMR_RECONFIGURATION(send_expand_all(&mat, &x, &res, &d, &z, &b), recv_expand_all(&mat, &x, &res, &d, &z, &b),
                            send_shrink_all(&mat, &x, &res, &d, &z, &b), recv_shrink_all(&mat, &x, &res, &d, &z, &b));
        free(sizes);
        free(dspls);
    }

    DMR_FINALIZE(remove_data(&mat, &x, &res, &d, &z, &b));

    return 0;
}
